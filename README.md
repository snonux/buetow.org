The foo.zone internet site
===========================

This repository contains the static files of my internet site.  [gemini://foo.zone](gemini://foo.zone) and [https://foo.zone](https://foo.zone).

Each format is in it's own branch in this repository. E.g.:

* Gemtext is in `content-gemtext`
* HTML is in `content-html`
* Markdown is in `content-md`
* ... ando so on.

You can find more about my internet site and the static content generator at [snonux/gemtexter](https://codeberg.org/snonux/gemtexter).

## Software I use to maintain this site

* Text editor: [Neovim](https://www.neovim.io)
* GNU Bash: [https://www.gnu.org/software/bash/](https://www.gnu.org/software/bash/)
* ShellCheck: [https://www.shellcheck.net/](https://www.shellcheck.net/)
* Server OS: [OpenBSD](https://www.OpenBSD.org)
* Gemini server: `vger` on OpenBSD via `relayd` and `inetd`.
* Web server: `httpd` of OpenBSD) (for "normal" HTML site)
* Obviously, I am also using Codeberg for source code and Codeberg Page hosting.
